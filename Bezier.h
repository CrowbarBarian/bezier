//Copyright 2018 CrowbarBarian (crowbarbarian(AT)outlook.com)

//Permission to use, copy, modify, and/or distribute this software for any purpose
//with or without fee is hereby granted, provided that the above copyright notice
//and this permission notice appear in all copies.

//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
//AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
//INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
//OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
//TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#ifndef BEZIER_H
#define BEZIER_H

#define DEFAULT_BEZIER_GRANULARITY 256

namespace Bezier {

//return values
class CubicCoeff
{
public:
	CubicCoeff(double a = 0.0, double b = 0.0, double c = 0.0, double d = 0.0) { this->A = a; this->B = b; this->C = c; this->D = d; }
	double A;
	double B;
	double C;
	double D;
};

class Cubic
{
public:
	Cubic(const int granularity = DEFAULT_BEZIER_GRANULARITY, const bool slow = false, const bool interp = true);
	Cubic(const Cubic& b);
	~Cubic();

	//of dubious use, really...but cppcheck complains, so we include it
	Cubic& operator=(const Cubic& b);
	
	//info functions
	int GetGranularity() { return steps; }
	bool isInterpolated() { return useInterp; }
	bool isSlow() { return useSlow; }
	
	//returns cubic Bezier terms as A, B, C, D. Uses caching and interpolation by default
	//multiply your 4 control points by these corresponding 4 factors and sum for final value
	CubicCoeff GetCoeffs(const double t);
		
private:
	inline CubicCoeff GetCachedCoeffs(const int idx); //get raw cached values, used internally only
	inline CubicCoeff SlowCalc(const double t); //use actual formula for the Bezier terms. uncached
	void PrecalcCubic(); //generate the cache
	bool useSlow; //always use the uncached calculation?
	bool useInterp; //use interpolation between intermediate cached values?
	int steps; //how many cached values we use. Only settable in constructors or via assignment operator
	double* C; //cached C term
	double* D; //cached D term
};

} //namespace Bezier
#endif //BEZIER_H