//Copyright 2018 CrowbarBarian (crowbarbarian(AT)outlook.com)

//Permission to use, copy, modify, and/or distribute this software for any purpose
//with or without fee is hereby granted, provided that the above copyright notice
//and this permission notice appear in all copies.

//THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
//WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY 
//AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
//INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
//OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
//TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

#include <cmath> //for modf()
#include "Bezier.h"

namespace Bezier {

Cubic::Cubic(const int granularity, const bool slow, const bool interp)
{
	this->useSlow = slow;
	this->useInterp = interp; 
	if(granularity < 0) this->steps = DEFAULT_BEZIER_GRANULARITY; //safe default value
	else this->steps = granularity;
	this->C = new double[steps];
	this->D = new double[steps];
	this->PrecalcCubic();
}

// copy ctor
Cubic::Cubic(const Cubic& b)
{
	this->steps = b.steps;
	this->useSlow = b.useSlow;
	this->useInterp = b.useInterp;
	this->C = new double[this->steps];
	this->D = new double[this->steps];
	for(int i = 0; i < this->steps; i++)
	{
		this->C[i] = b.C[i];
		this->D[i] = b.D[i];
	}
}

Cubic& Cubic::operator=(const Cubic& b)
{
	if(this == &b) return *this;
	if(this->steps == b.steps)
	{
		//no need to copy cache, will be the same
		this->useSlow = b.useSlow;
		this->useInterp = b.useInterp;
	}
	else
	{
		this->steps = b.steps;
		this->useSlow = b.useSlow;
		this->useInterp = b.useInterp;
		{ //ok, using new step value, copy cache from b
			delete[] this->C;
			delete[] this->D;
			this->C = new double[this->steps];
			this->D = new double[this->steps];
			for(int i = 0; i < this->steps; i++)
			{
				this->C[i] = b.C[i];
				this->D[i] = b.D[i];
			}
		}
	}
	return *this;
}

Cubic::~Cubic()
{
	delete[] C;
	delete[] D;
}

// this function caches the results of the cubic Bezier coefficients
// for use in the GetCachedCoeffs() function
// steps is the size of the arrays C and D
void Cubic::PrecalcCubic()
{
	double Increment = 1.0 / (this->steps - 1); //instead of a divide each loop, just add a delta
	double t = 0.0;
	
	//precalculate the C and D terms and store them in an array
	for(int i = 0; i < this->steps; i++)
	{
		double t2 = t * t;
		this->C[i] = 3 * (1.0 - t) * t2;
		this->D[i] = t * t2;
		t += Increment;
	}
}

CubicCoeff Cubic::GetCachedCoeffs(const int idx)
{
	//mirror idx
	int mirroredidx = (this->steps - 1) - idx;
	
	return CubicCoeff(this->D[mirroredidx],
						this->C[mirroredidx],
						this->C[idx],
						this->D[idx]);
}

CubicCoeff Cubic::SlowCalc(const double t)
{
	double omt = 1.0 - t; //one minus t
	double omt2 = omt * omt; //one minus t squared
	double t2 = t * t; //t squared
		
	return CubicCoeff(omt * omt2,
						3 * omt2 * t,
						3 * omt * t2,
						t * t2);
}

CubicCoeff Cubic::GetCoeffs(const double t)
{
	//instead of throwing an out-of-range exception, just roll with it (technically this works, so...)
	//gotta use SlowCalc(), as it works with any value of t
	if(useSlow || (t < 0.0) || (t > 1.0)) return this->SlowCalc(t);
	
	//so, t is between 0.0 and 1.0, use the cache
	int maxidx = this->steps - 1;
	double expand = t * maxidx; //map 0.0 <-> 1.0 to 0.0 <-> maxidx
	
	//separate integer and fractional components of expand
	//use integer for index into arrays
	//use fraction for interpolation factor
	double index;
	double interp = modf(expand, &index);
	int idx = static_cast<int>(index);
	
	//mirror the range of the interpolation value
	double omi = 1.0 - interp;
	//mirror the index to the A and B terms, so that the range is maxidx <-> 0
	int reverseidx = maxidx - idx;
		
	//if interp is 0.0, we're dead on a cached value, no need to interpolate
	if(!useInterp || (interp == 0.0)) return this->GetCachedCoeffs(idx);
	
	//calculate the coefficients
	//the trick is that the A and B terms are just mirrors of the C and D terms along the x axis
	//basically, A(t) == D(1.0 - t) and B(t) == C(1.0 - t)
	//we precalculated the C and D terms, so just mirror the remapped index for A and B
		
	return CubicCoeff(this->D[reverseidx - 1] * interp + this->D[reverseidx] * omi,
						this->C[reverseidx - 1] * interp + this->C[reverseidx] * omi,
						this->C[idx+1] * interp + this->C[idx] * omi,
						this->D[idx+1] * interp + this->D[idx] * omi);
}

} //namespace Bezier