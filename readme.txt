The Bezier curve library, by CrowbarBarian (crowbarbarian(AT)outlook.com) (c)2018

A simple pair of classes to compute the Bezier coefficients of a cubic Bezier curve.

Class Cubic is the workhorse, and has all of the functionality. CubicCoeff is a simple container class to hold the results of a query.

How to use the Bezier library:

Just dump Bezier.h and Bezier.cpp into your project somewhere, #include "Bezier.h" in the code files you want to use the Bezier class in.
Then, just instantiate a Bezier object somewhere, like so:

Bezier::Cubic b1;

You can get fancy by adding constructor parameters:

Bezier::Cubic b2(2048, false, true);

the first parameter is the granularity of the Cubic Bezier cache, i.e. how many entries. The bigger the number, the more entries and a smoother curve, but that also means longer initialization times and more memory use.
The second parameter is if you want to disable the cache, A value of "true" means disable the cache (use the slow version).
The third parameter is to enable linear interpolation of the cached values. You should just leave this as "true", unless you really, *REALLY* know what you're doing.

Once you have a instance of the Bezier object, you can query it for the Bezier coefficients via the GetCoeffs() function.

Here is a sample program to demonstrate:

#include <iostream>
#include "Bezier.h"

using namespace std;

int main(int argc, char **argv)
{
	const int grains = 256;
	Bezier::Cubic b1(grains);
	for(int i = 0; i < grains; i++)
	{
		Bezier::CubicCoeff Coeff;
		double t = i / static_cast<double>(grains - 1);
		Coeff = b1.GetCoeffs(t);
		cout << "When t is " << t << ", " << endl;
		cout << "A is " << Coeff.A << ", " << endl;
		cout << "B is " << Coeff.B << ", " << endl;
		cout << "C is " << Coeff.C << ", " << endl;
		cout << "and D is " << Coeff.D << "." << endl << endl;
	}
	return 0;
}

This will barf out a whole bunch of numbers. Not very useful by itself; you need some numbers of your own to make everything work.
What you will need are 4 points: vectors of 1D, 2D, 3D, heck, even 4D and above can work. Points 1 and 4 are the endpoints, where the curve begins and ends.
Points 2 and 3 are guide points, and control how the curve bends; refer to https://en.wikipedia.org/wiki/B%C3%A9zier_curve for the messy technical details.
The important thing is that each control point has a corresponding coefficient that it needs to be multiplied by to achieve the desired result. Point 1 needs to be
multiplied by coefficient A, 2 by B, 3 by C, and 4 by D. Then you add all of the results together to produce the resultant point on the curve determined by the distance factor, "t",
which ranges from 0.0 to 1.0. 0.0 gives the point closest to the start point, and 1.0 is for the point closest to the end point. Inbetween values use the control points to influence
how the curve bends.

Here's a little code snippet:

class Vector3
{
public:
	Vector3(double x, double y, double z) { this->x = x; this->y = y; this->z = z; }

	double x;
	double y;
	double z;
};

Vector3 GetPoint(const Bezier::Cubic& b, double t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
{
	Bezier::CubicCoeff Coeff = b.GetCoeffs(t);

	return Vector3(Coeff.A * p1.x + Coeff.B * p2.x + Coeff.C * p3.x + Coeff.D * p4.x,
			Coeff.A * p1.y + Coeff.B * p2.y + Coeff.C * p3.y + Coeff.D * p4.y,
			Coeff.A * p1.z + Coeff.B * p2.z + Coeff.C * p3.z + Coeff.D * p4.z);
}

What happens here is the result Vector3's "x" is a sum of points 1 to 4's "x" coordinate multiplied by coefficients A to D. The same goes for the y and z coordinates. If you use operator overrides
for "*" and "+" for your vectors you can even simplify it further:

class Vector3
{
public:
	Vector3(double x, double, y, double z) { this->x = x; this->y = y; this->z = z; }

	Vector3 operator+(const Vector3& rhs);
	Vector3 operator*(const double f);

	double x;
	double y;
	double z;
};

Vector3 Vector3::operator+(const Vector3& rhs)
{
	return Vector3(this->x + rhs.x, this->y + rhs.y, this->z + rhs.z);
}

Vector3 Vector3::operator*(const double f)
{
	return Vector3(this->x * f, this->y * f, this->z * f);
}

Vector3 GetPoint(Bezier::Cubic& b, double t, Vector3 p1, Vector3 p2, Vector3 p3, Vector3 p4)
{
	Bezier::CubicCoeff Coeff = b.GetCoeffs(t);

	return p1 * Coeff.A + p2 * Coeff.B + p3 * Coeff.C + p4 * Coeff.D;
}

And that's really all there is to using the Bezier library!

Developed with info from https://en.wikipedia.org/wiki/B%C3%A9zier_curve
